<?php get_header() ?>
	
	<div id="wrapper">
		<div id="container" class="col-md-8">

            <?php while ( have_posts() ) : the_post() ?>

				<div id="post-<?php the_ID() ?>" <?php post_class(); ?>>
					<h2 class="entry-title"><a href="<?php the_permalink() ?>" title="<?php the_title(); ?>"><?php the_title() ?></a></h2>
					<div class="entry-date"><span class="published" title="<?php the_time('Y-m-d\TH:i:sO') ?>"><?php the_time('Y-m-d\TH:i:sO') ?></span></div>
					<div class="entry-content">
	                    <?php the_excerpt(); ?>
					</div>
				</div><!-- .post -->

            <?php endwhile; ?>

			<div id="nav-below" class="navigation">
				<div class="nav-previous"><?php next_posts_link(__('<span class="meta-nav">&laquo;</span> Older posts')) ?></div>
				<div class="nav-next"><?php previous_posts_link(__('Newer posts <span class="meta-nav">&raquo;</span>')) ?></div>
			</div>
			
		
		</div><!-- #container -->
		<?php get_sidebar() ?>
	</div><!-- #wrapper -->

<?php get_footer() ?>