<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php wp_title( '-', true, 'right' ); ?><?=wp_specialchars( get_bloginfo('name'), 1 )?></title>
        <?php if (is_page(11)) { ?>
            <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
        <?php } ?>
        <!-- Bootstrap -->
        <link href="<?php bloginfo('template_url'); ?>/css/bootstrap.min.css" rel="stylesheet">
        <!-- Theme Stylesheet -->
        <link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_url') ?>">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,400italic|Montserrat|Crimson+Text:400italic,700italic' rel='stylesheet' type='text/css'>
        <link rel="apple-touch-icon" sizes="57x57" href="/favicons/apple-touch-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="/favicons/apple-touch-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="/favicons/apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="/favicons/apple-touch-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="/favicons/apple-touch-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="/favicons/apple-touch-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="/favicons/apple-touch-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="/favicons/apple-touch-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="/favicons/apple-touch-icon-180x180.png">
        <link rel="icon" type="image/png" href="/favicons/favicon-32x32.png" sizes="32x32">
        <link rel="icon" type="image/png" href="/favicons/android-chrome-192x192.png" sizes="192x192">
        <link rel="icon" type="image/png" href="/favicons/favicon-96x96.png" sizes="96x96">
        <link rel="icon" type="image/png" href="/favicons/favicon-16x16.png" sizes="16x16">
        <link rel="manifest" href="/favicons/manifest.json">
        <meta name="msapplication-TileColor" content="#da532c">
        <meta name="msapplication-TileImage" content="/favicons/mstile-144x144.png">
        <meta name="theme-color" content="#ffffff">
        <?php wp_head(); ?>
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body <?php body_class(); ?> <?=is_front_page()?'id="home_body"':''?>>
        <?php
            $phone_number = get_option('phone_number');
            $email = get_option('email');
            $fb_url = get_option('fb_url');
            $tw_url = get_option('tw_url');
            $pt_url = get_option('pt_url');
            $ig_url = get_option('ig_url');
            $address = get_option('address');
        ?>
        <div id="mobile_header_wrapper">
            <div id="mobile_header" class="container text-center">
                <a id="mobile_logo" href="<?=home_url()?>/parties/" title="<?=wp_specialchars( get_bloginfo('name'), 1 )?>">
                    <img src="<?php bloginfo('template_url'); ?>/images/svgs/homepage_logo_mobile.svg">
                </a>
                <span id="toggle_mobile_nav" class="pull-right"></span>
            </div>
        </div>
        <div id="mobile_menu_wrapper" class="text-center">
            <div id="mobile_menu">
                <?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu' => 'mainmenu' ) ); ?>
            </div>
            <div id="mobile_info">
                General Info:&nbsp;&nbsp;<a href="tel:<?=$phone_number?>" title="<?=$phone_number?>">504.208.5569</a><br>
                Event Inquiries:&nbsp;&nbsp;<a href="tel:504.208.5593" title="504.208.5593">504.208.5593</a><br>
                <a href="https://www.google.hu/maps/place/600+Fulton+St,+New+Orleans,+LA+70130,+Egyes%C3%BClt+%C3%81llamok/@29.946616,-90.0651344,17z/data=!3m1!4b1!4m2!3m1!1s0x8620a66d8abb12d1:0x662d4284677ff484?hl=hu" id="mobile_header_address"><?=$address?></a>
            </div>
            <div id="mobile_social">
                <a href="<?=$tw_url?>" title="Twitter" class="msoc" id="mtw" target="_blank"></a>
                <a href="<?=$fb_url?>" title="Facebook" class="msoc" id="mfb" target="_blank"></a>
                <a href="<?=$pt_url?>" title="Pinterest" class="msoc" id="mpt" target="_blank"></a>
                <a href="<?=$ig_url?>" title="Instagram" class="msoc" id="mig" target="_blank"></a>
                <a href="mailto:<?=$email?>" title="Email" class="msoc" id="mmail"></a>
            </div>
        </div>
        <div id="header-wrap" class="be-header-wrap">
            <header id="header" class="container be-header">
                <div class="row">
                    <div class="col-sm-2">
                        <a href="<?=home_url()?>/parties/" id="logo" title="<?=wp_specialchars( get_bloginfo('name'), 1 )?>">
                            <img src="<?php bloginfo('template_url'); ?>/images/be_logo.png" alt="<?=wp_specialchars( get_bloginfo('name'), 1 )?>">
                        </a>
                    </div>
                    <div class="col-sm-10">
                        <div id="header_info" class="be-header_info">
                            <?php
                                $phone_number = get_option('phone_number');
                                $address = get_option('address'); ?>
                            <a href="tel:504-208-5593" title="504.208.5593">504.208.5593</a> | <?=$address?>
                        </div>
                    </div>
                </div>
            </header>
        </div>