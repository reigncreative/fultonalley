<?php
	/**
	 * Template Name: Gallery
	 */
?>

<?php get_header() ?>
<?php the_post(); ?>
	<div id="top_slider">
		<ul class="bxslider">
		  <li><div class="top_slider_slide" style="background-image: url('<?=get_thumbnail($post->ID, 'full')?>');"></div></li>
		  <?php for ($i=2; $i<=8; $i++) {
		  	if (get_post_meta($post->ID, 'featured_image_'.$i, true)) {
		  		$image_post = get_post(get_post_meta($post->ID, 'featured_image_'.$i, true)); ?>
		  	<li><div class="top_slider_slide" style="background-image: url('<?=$image_post->guid?>');"></div></li>
		  <?php } } ?>
		</ul>
		<div id="top_image_wrapper" class="absolute_text">
			<div id="top_image" class="container text-center">
				<h1 id="top_title"><?=get_post_meta($post->ID, 'top_title', true)?></h1>
				<div id="top_text"><?=wpautop(get_post_meta($post->ID, 'top_text', true))?></div>
			</div>
		</div>
	</div>
	<div id="wrapper" class="container">
		<div id="gallery_category_list">
			<ul id="filters" class="option-set clearfix" data-option-key="filter">
                <li><a href="" data-option-value="*" class="selected" title="All">All</a></li>
                <?php
                    $terms = get_terms("gallery-cat");
                    foreach ($terms as $term) { ?>
                        <li><a href="javascript:;" data-option-value=".p<?=$term->slug?>" title="<?=$term->name?>"><?=$term->name?></a></li>
                    <?php }
                ?>
            </ul>
            <div class="clear"></div>
		</div>
		<div id="gallery_content">
			<ul id="portfolio_ul" class="rows">
			<?php
				global $post;
				$args = array('posts_per_page' => 9999, 'post_type'=> 'gallery-item');
				$myposts = get_posts( $args );
				$images = get_instagram_images(50);
				foreach ( $myposts as $post ) : 
				  	setup_postdata( $post );
				  	$terms = "";
                    $term_list = wp_get_post_terms($post->ID, 'gallery-cat', array("fields" => "all"));
                    if ($term_list) {
                        foreach ($term_list as $term) {
                            $terms .= $term->slug;
                        }
                    } ?>
					<li class="portfolio_item col-sm-4 p<?=$terms?> <?=$i%3==0?"last_portfolio":""?>" data-post="<?=$post->ID?>" data-category="<?=$terms?>">
						<?php
							switch ($terms) {
								case '360-tours': {
									$image = get_thumbnail($post->ID, 'full'); ?>
									<a href="#" class="gal_image fancy">
										<img src="<?=$image?>" alt="<?php the_title();?>">
										<span class="gal_360">View 360<sup>o</sup> tour</span>
									</a>
									<div class="gal_text">
										<div class="gal_text_inner"><?php the_content(); ?></div>
									</div>
								<?php }; break;
								case 'photos': {
									$instagram_image = get_post_meta($post->ID, 'insta_url', true);
									$insta_text = get_post_meta($post->ID, 'insta_text', true);
									$insta_user = get_post_meta($post->ID, 'insta_user', true);
									$text = '';
									$user = '';
									$image = get_thumbnail($post->ID, 'full');
									if (!$image) {
										/*foreach ($images as $inst_image) {
											//var_dump($inst_image);
											if ($inst_image->fullres == $instagram_image) {
												$image = $instagram_image;
												$text = $inst_image->text;
												$user = $inst_image->user;
												break;
											}
										}*/ ?>
										<a href="<?=$instagram_image?>" class="gal_image fancy">
											<img src="<?=$instagram_image?>" alt="<?php the_title();?>">
											<span class="gal_insta"></span>
										</a>
										<div class="gal_text">
											<div class="gal_text_inner"><?=strip_tags($insta_text)?></div>
											<div class="gal_insta_author">Photo by <?=$insta_user?></div>
										</div>
										<?php $image = ''; ?>
									<?php } else {
										
										?>
										<a href="<?=$image?>" class="gal_image fancy">
											<img src="<?=$image?>" alt="<?php the_title();?>">
										</a>
									<?php } ?>
								<?php }; break;
								case 'videos': {
									$image = get_thumbnail($post->ID, 'full'); ?>
									<a href="<?=get_post_meta($post->ID, 'video_url', true)?>" class="gal_image fancy">
										<img src="<?=$image?>" alt="<?php the_title();?>">
										<span class="gal_video"></span>
									</a>
									<div class="gal_text">
										<div class="gal_text_inner"><?php the_content(); ?></div>
									</div>
								<?php }; break;
							}
						?>
						
					</li>
				<?php endforeach;
				wp_reset_postdata(); ?>
			</ul>
		</div>
	</div><!-- #wrapper -->
<?php get_footer() ?>