	<div id="info_top">
		<div id="info_top_inner"></div>
	</div>
	<div id="info_wrapper">
		<div id="info" class="container">
			<div id="info_cols">
				<div id="info_1">
					<img src="<?php bloginfo('template_url'); ?>/images/info_logo.png" alt="<?=wp_specialchars( get_bloginfo('name'), 1 )?>">
				</div>
				<div id="info_2" class="text-center">
					<?php $address = get_option('address');
					$phone_number = get_option('phone_number'); ?>
					<div class="footer_address"><?=$address?></div>
					<a href="tel:<?=$phone_number?>" id="reservation_link" title="Get in Touch"><img src="<?php bloginfo('template_url'); ?>/images/info_phone.png" alt="Get in Touch"></a>
				</div>
				<div id="info_3">
					<div id="info_3_top">
						<div class="row">
							<div class="col-sm-4 text-center">
								<span class="open_days">Mon-Thurs</span>
								<span class="open_times">4pm - Midnight</span>
							</div>
							<div class="col-sm-4 text-center">
								<span class="open_days">Fri-Sat</span>
								<span class="open_times">11am - 2am</span>
							</div>
							<div class="col-sm-4 text-center">
								<span class="open_days">Sundays</span>
								<span class="open_times">11am - Midnight</span>
							</div>
						</div>
					</div>
					<div id="info_3_bottom" class="text-center">
						<div id="info_3_bottom_top">We welcome bowlers under 21 before 8pm.</div>
						<div id="info_3_bottom_bottom">Lane rentals are on a first-come, first-served basis.</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<div id="info_bottom">
		<div id="info_bottom_inner">
			<div class="container text-center">We offer discounted parking [2 hours for $4, 4 hours for $8] in the garage at 501 Convention Center [and the 601 Convention Center garage on weekends].</div>
		</div>
	</div>
	<div id="footer-wrap">
		<footer id="footer" class="container">
			<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Footer widgets") ) : ?>
        	<?php endif; ?>
		</footer>
	</div>
	<div id="mobile_footer_social" class="text-center">
		<?php
			$fb_url = get_option('fb_url');
	        $tw_url = get_option('tw_url');
	        $pt_url = get_option('pt_url');
	        $ig_url = get_option('ig_url');
	        $email = get_option('email');
		?>
		<a href="<?=$tw_url?>" title="Twitter" class="mfsoc" id="mftw" target="_blank"></a>
	    <a href="<?=$fb_url?>" title="Facebook" class="mfsoc" id="mffb" target="_blank"></a>
	    <a href="<?=$pt_url?>" title="Pinterest" class="mfsoc" id="mfpt" target="_blank"></a>
	    <a href="<?=$ig_url?>" title="Instagram" class="mfsoc" id="mfig" target="_blank"></a>
	    <a href="mailto:<?=$email?>" title="Email" class="mfsoc" id="mfmail"></a>
	</div>
	<div id="footer-bottom-wrap">
		<footer id="footer-bottom" class="container text-center">
			Copyright &copy; 2016 Fulton Alley | Designed by <a href="http://www.rapjab.com" target="_blank" title="RapJab">RapJab</a>
		</footer>
	</div>

	<?php
		date_default_timezone_set('America/Chicago');
		$event_popup_active = false;
		$current_time = time();
		global $post;
		$args = array('posts_per_page' => 9999, 'post_type'=> 'event', 'meta_query' => array(
			array(
				'key' => 'buyout',
				'value' => 'yes',
			)
		));
		$myposts = get_posts( $args );
		foreach ( $myposts as $post ) : 
		  	setup_postdata( $post );
		  	$fields = get_fields($post->ID);
		  	$buyout_start = strtotime($fields['date'].' '.$fields['buyout_start']);
		  	$buyout_end = strtotime($fields['date'].' '.$fields['buyout_end']);
		  	if (($buyout_start <= $current_time) && ($buyout_end >= $current_time)) {
		  		$event_popup_active = true;
		  		break;
		  	}
	  	endforeach;
		wp_reset_postdata(); ?>
		<?php if ($event_popup_active) { ?>
			<div id="event_popup" class="hidden">
				<div id="event_popup_close">X</div>
			</div>
		<?php } else { ?>
		<?php } ?>
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php bloginfo('template_url'); ?>/js/bootstrap.min.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/jquery.bxslider.min.js" type="text/javascript"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/jquery.isotope.min.js" type="text/javascript"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/froogaloop2.min.js" type="text/javascript"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/jquery-ui.min.js" type="text/javascript"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/custom.js" type="text/javascript"></script>
    <script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	  ga('create', 'UA-34865935-24', 'auto');
	  ga('send', 'pageview');
	</script>

	<!--  Quantcast Tag -->
	<script>
	  qcdata = {} || qcdata;
	       (function(){
	       var elem = document.createElement('script');
	       elem.src = (document.location.protocol == "https:" ? "https://secure" : "http://pixel") + ".quantserve.com/aquant.js?a=p-tA9rj_4kF13pK";
	       elem.async = true;
	       elem.type = "text/javascript";
	       var scpt = document.getElementsByTagName('script')[0];
	       scpt.parentNode.insertBefore(elem,scpt);
	     }());


	   var qcdata = {qacct: 'p-tA9rj_4kF13pK',
	                        orderid: '',
	                        revenue: ''
	                        };
	</script>
	  <noscript>
	    <img src="//pixel.quantserve.com/pixel/p-tA9rj_4kF13pK.gif?labels=_fp.event.Default" style="display: none;" border="0" height="1" width="1" alt="Quantcast"/>
	  </noscript>
	<!-- End Quantcast Tag -->
	<?php if ((is_front_page()) || (is_page(7))) { ?>
		<script type="text/javascript" src="http://a.adtpix.com/px/?id=106931"></script>
	<?php } ?>
    <?php wp_footer(); ?>
  </body>
</html>