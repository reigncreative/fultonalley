	<div id="info_top">
		<div id="info_top_inner"></div>
	</div>
	<div id="footer-wrap">
		<footer id="footer" class="container">
			<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Events Footer Widgets") ) : ?>
        	<?php endif; ?>
		</footer>
	</div>
	<div id="mobile_footer_social" class="text-center">
		<?php
			$fb_url = get_option('fb_url');
	        $tw_url = get_option('tw_url');
	        $ig_url = get_option('ig_url');
	        $email = get_option('email');
		?>
		<a href="<?=$tw_url?>" title="Twitter" class="mfsoc" id="mftw" target="_blank"></a>
	    <a href="<?=$fb_url?>" title="Facebook" class="mfsoc" id="mffb" target="_blank"></a>
	    <a href="<?=$ig_url?>" title="Instagram" class="mfsoc" id="mfig" target="_blank"></a>
	    <a href="mailto:<?=$email?>" title="Email" class="mfsoc" id="mfmail"></a>
	</div>
	<div id="footer-bottom-wrap">
		<footer id="footer-bottom" class="container text-center">
			Copyright &copy; 2015 Fulton Allery | Designed by <a href="http://www.rapjab.com" target="_blank" title="RapJab">RapJab</a>
		</footer>
	</div>
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php bloginfo('template_url'); ?>/js/bootstrap.min.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/jquery.bxslider.min.js" type="text/javascript"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/jquery.isotope.min.js" type="text/javascript"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/froogaloop2.min.js" type="text/javascript"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/jquery-ui.min.js" type="text/javascript"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/custom.js" type="text/javascript"></script>
    <script type="text/javascript">
    	jQuery( "input[name=event_date]" ).datepicker({
			inline: true
		});
    </script>
    <script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	  ga('create', 'UA-34865935-24', 'auto');
	  ga('send', 'pageview');
	</script>

	<!--  Quantcast Tag -->
	<script>
	  qcdata = {} || qcdata;
	       (function(){
	       var elem = document.createElement('script');
	       elem.src = (document.location.protocol == "https:" ? "https://secure" : "http://pixel") + ".quantserve.com/aquant.js?a=p-tA9rj_4kF13pK";
	       elem.async = true;
	       elem.type = "text/javascript";
	       var scpt = document.getElementsByTagName('script')[0];
	       scpt.parentNode.insertBefore(elem,scpt);
	     }());


	   var qcdata = {qacct: 'p-tA9rj_4kF13pK',
	                        orderid: '',
	                        revenue: ''
	                        };
	</script>
	  <noscript>
	    <img src="//pixel.quantserve.com/pixel/p-tA9rj_4kF13pK.gif?labels=_fp.event.Default" style="display: none;" border="0" height="1" width="1" alt="Quantcast"/>
	  </noscript>
	<!-- End Quantcast Tag -->
	<script type="text/javascript" src="http://a.adtpix.com/px/?id=106931"></script>
	<script>
	function dropPixels() {
	 var segmentPixel = document.createElement("script");
	 segmentPixel.src = "https://a.adtpix.com/px/?id=106932";
	 document.body.appendChild(segmentPixel);
	 
	 var conversionPixel = document.createElement("script");
	 conversionPixel.src = "https://a.adtpix.com/px/?id=106933";
	 document.body.appendChild(conversionPixel);
	}
	</script>

    <?php wp_footer(); ?>
  </body>
</html>