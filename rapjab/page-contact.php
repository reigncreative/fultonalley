<?php
	/**
	 * Template Name: Contact
	 */
?>

<?php get_header() ?>
<?php the_post(); ?>
	<div id="google_map"></div>
	<div id="wrapper" class="container">
		<div class="row">
			<div id="container" class="col-sm-12">
				<div id="contact_top" class="row">
					<?php for ($i=1; $i<=3; $i++) { ?>
						<div class="col-sm-4 text-center contact_li">
							<h3 class="contact_title"><?=get_post_meta($post->ID, 'contact_title_'.$i, true)?></h3>
							<div class="contact_text"><?=wpautop(get_post_meta($post->ID, 'contact_text_'.$i, true))?></div>
						</div>
					<?php } ?>
					<div class="clearfix"></div>
				</div>
				<?php /*<div id="contact_bottom" class="text-center">
					<h3>Looking to connect with us on Social Media?</h3>
					<?php
                        $fb_url = get_option('fb_url');
                        $tw_url = get_option('tw_url');
                        $pt_url = get_option('pt_url');
                        $ig_url = get_option('ig_url');
                    ?>
                    <div class="row">
                    	<div class="col-xs-1 col-xs-offset-4">
                    		<a href="<?=$tw_url?>" title="Twitter" class="csoc" id="ctw" target="_blank"></a>
                    	</div>
                    	<div class="col-xs-1">
                    		<a href="<?=$fb_url?>" title="Facebook" class="csoc" id="cfb" target="_blank"></a>
                    	</div>
                    	<div class="col-xs-1">
                    		<a href="<?=$pt_url?>" title="Pinterest" class="csoc" id="cpt" target="_blank"></a>
                    	</div>
                    	<div class="col-xs-1">
                    		<a href="<?=$ig_url?>" title="Instagram" class="csoc" id="cig" target="_blank"></a>
                    	</div>
                    </div>
				</div>*/ ?>
				<div id="contact_bottom_row" class="row text-center">
					<div class="col-sm-6">
						<h3>Looking for parking?</h3>
						<div id="parking_content"><?=wpautop(get_post_meta($post->ID, 'parking_text', true))?></div>
					</div>
					<div class="col-sm-6">
						<h3>Looking to connect with us on Social Media?</h3>
						<?php
	                        $fb_url = get_option('fb_url');
	                        $tw_url = get_option('tw_url');
	                        $pt_url = get_option('pt_url');
	                        $ig_url = get_option('ig_url');
	                    ?>
	                    <div class="row">
	                    	<div class="col-xs-2 col-xs-offset-2">
	                    		<a href="<?=$tw_url?>" title="Twitter" class="csoc" id="ctw" target="_blank"></a>
	                    	</div>
	                    	<div class="col-xs-2">
	                    		<a href="<?=$fb_url?>" title="Facebook" class="csoc" id="cfb" target="_blank"></a>
	                    	</div>
	                    	<div class="col-xs-2">
	                    		<a href="<?=$pt_url?>" title="Pinterest" class="csoc" id="cpt" target="_blank"></a>
	                    	</div>
	                    	<div class="col-xs-2">
	                    		<a href="<?=$ig_url?>" title="Instagram" class="csoc" id="cig" target="_blank"></a>
	                    	</div>
	                    </div>
					</div>
				</div>
			</div><!-- #container -->
			<?php //get_sidebar() ?>
		</div>
	</div><!-- #wrapper -->
<?php get_footer() ?>