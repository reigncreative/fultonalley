<?php
	/**
	 * Template Name: Better Events
	 */
?>

<?php get_template_part('header-betterevents') ?>
<?php the_post(); ?>
	<div id="top_image_wrapper" style=" background-image: url('<?=get_thumbnail($post->ID, 'full')?>');">
		<div id="top_image" class="container text-center">
			<h1 id="top_title"><?=get_post_meta($post->ID, 'top_title', true)?></h1>
			<div id="top_text"><?=wpautop(get_post_meta($post->ID, 'top_text', true))?></div>
		</div>
	</div>
	<div id="wrapper" class="container">
		<div class="row">
			<div class="col-sm-6 better_events_left">
				<h2 class="entry-title normal_page_title"><?=get_post_meta($post->ID, 'block_1_title', true)?></h2>
				<div class="entry-content normal_page_content"><?=wpautop(get_post_meta($post->ID, 'block_1_text', true))?></div>
				<div id="be_slider">
					<ul class="bxslider">
						<?php for ($i=1; $i<=10; $i++) {
							if (get_post_meta($post->ID, 'block_'.$i.'_image', true)) {
								$image = get_post(get_post_meta($post->ID, 'block_'.$i.'_image', true)); ?>
								<li><img src="<?=$image->guid?>" alt="Better Events"></li>
							<?php } ?>
						<?php } ?>
					</ul>
				</div>
			</div>
			<div class="col-sm-5 col-sm-offset-1 better_events_right">
				<div class="row">
					<div class="col-xy-12">
						<h3><?=get_post_meta($post->ID, 'block_2_title', true)?></h3>
						<h4><?=get_post_meta($post->ID, 'block_2_secondary_title', true)?></h4>
						<div class="better_events_right_text"><?=wpautop(get_post_meta($post->ID, 'block_2_text', true))?></div>
					</div>
				</div>
				<div id="better_events_form">
					<script type="text/javascript">var submitted = false;</script>
					<iframe id="hidden_iframe" onload="if(submitted) { jQuery('#message_sent').fadeIn(); jQuery('#ss-form')[0].reset(); dropPixels(); }" style="display:none;" name="hidden_iframe"></iframe>
					<form id="ss-form" onsubmit="submitted=true;" target="hidden_iframe" method="POST" action="<?=home_url()?>/wp-content/plugins/fulton/ajax.php?add_lead=1" novalidate="novalidate">
						<div class="row">
							<div class="col-xs-6">
								<label>First Name*</label><span class="wpcf7-form-control-wrap first-name"><input type="text" aria-invalid="false" aria-required="true" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" size="40" value="" id="firstname" name="firstname"></span>
							</div>
							<div class="col-xs-6">
								<label>Last Name*</label>
								<span class="wpcf7-form-control-wrap last-name"><input type="text" aria-invalid="false" aria-required="true" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" size="40" value="" name="lastname" id="lastname"></span>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-6">
								<label>Email Address*</label>
								<span class="wpcf7-form-control-wrap your-email"><input type="email" aria-invalid="false" aria-required="true" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" size="40" value="" name="youremail" id="youremail"></span>
							</div>
							<div class="col-xs-4">
								<label>Phone*</label>
								<span class="wpcf7-form-control-wrap phone"><input type="text" aria-invalid="false" aria-required="true" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" size="40" value="" name="phone" id="phone"></span>
							</div>
							<div class="col-xs-2">
								<label>Ext</label>
								<span class="wpcf7-form-control-wrap ext"><input type="text" aria-invalid="false" class="wpcf7-form-control wpcf7-text" size="40" value="" name="ext" id="ext"></span>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12">
								<label>Company Name</label>
								<span class="wpcf7-form-control-wrap company"><input type="text" aria-invalid="false" class="wpcf7-form-control wpcf7-text" size="40" value="" name="company" id="company"></span>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12">
								<label>Zipcode*</label>
								<span class="wpcf7-form-control-wrap zip"><input type="text" aria-invalid="false" class="wpcf7-form-control wpcf7-text" size="40" value="" name="zip" id="zip"></span>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12">
								<label>Type of event</label>
								<div class="select_dropdown">
									<span class="wpcf7-form-control-wrap event"><select aria-invalid="false" class="wpcf7-form-control wpcf7-select" name="event" id="event"><option value=""></option><option value="Corporate Event">Corporate Event</option></select></span>
								</div>
						</div>
						</div>
						<div class="row">
							<div class="col-xs-4">
								<label>Event Date*</label>
								<span class="wpcf7-form-control-wrap event_date"><input type="text" aria-invalid="false" aria-required="true" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" size="40" value="" name="event_date" id="event_date"></span>
							</div>
							<div class="col-xs-4">
								<label>Start Time</label>
								<span class="wpcf7-form-control-wrap start_time"><input type="text" aria-invalid="false" class="wpcf7-form-control wpcf7-text" size="40" value="" name="start_time" id="start_time"></span>
							</div>
							<div class="col-xs-4">
								<label>End Time</label>
								<span class="wpcf7-form-control-wrap end_time"><input type="text" aria-invalid="false" class="wpcf7-form-control wpcf7-text" size="40" value="" name="end_time" id="end_time"></span>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-4">
								<label>Number of Attendees</label>
								<span class="wpcf7-form-control-wrap attendees"><input type="text" aria-invalid="false" class="wpcf7-form-control wpcf7-text" size="40" value="" name="attendees" id="attendees"></span>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12">
								<label>Additional Information</label>
								<span class="wpcf7-form-control-wrap your-message"><textarea aria-invalid="false" class="wpcf7-form-control wpcf7-textarea" rows="10" cols="40" name="yourmessage" id="yourmessage"></textarea></span>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12">
								<input type="submit" class="wpcf7-form-control wpcf7-submit" value="Submit Inquiry">
							</div>
						</div>
						<div id="circularG" class="ajax_loader">
							<div id="circularG_1" class="circularG"></div>
							<div id="circularG_2" class="circularG"></div>
							<div id="circularG_3" class="circularG"></div>
							<div id="circularG_4" class="circularG"></div>
							<div id="circularG_5" class="circularG"></div>
							<div id="circularG_6" class="circularG"></div>
							<div id="circularG_7" class="circularG"></div>
							<div id="circularG_8" class="circularG"></div>
						</div>
						<div id="message_sent">Thanks for sending over your details. One of our event staff will be in touch shortly.</div>
					</form>
				</div>
			</div>
		</div>
	</div><!-- #wrapper -->
<?php get_template_part('footer-betterevents') ?>