<?php
	/**
	 * Template Name: Menu
	 */
?>

<?php get_header() ?>
<?php the_post(); ?>
	<div id="top_slider">
		<ul class="bxslider">
		  <li><div class="top_slider_slide" style="background-image: url('<?=get_thumbnail($post->ID, 'full')?>');"></div></li>
		  <?php for ($i=2; $i<=8; $i++) {
		  	if (get_post_meta($post->ID, 'featured_image_'.$i, true)) {
		  		$image_post = get_post(get_post_meta($post->ID, 'featured_image_'.$i, true)); ?>
		  	<li><div class="top_slider_slide" style="background-image: url('<?=$image_post->guid?>');"></div></li>
		  <?php } } ?>
		</ul>
		<div id="top_image_wrapper" class="absolute_text">
			<div id="top_image" class="container text-center">
				<h1 id="top_title"><?=get_post_meta($post->ID, 'top_title', true)?></h1>
				<div id="top_text"><?=wpautop(get_post_meta($post->ID, 'top_text', true))?></div>
			</div>
		</div>
	</div>
	<div id="menu_nav_wrapper">
		<div id="menu_nav">
			<div id="menu_nav_inner" class="container">
				<ul>
					<?php
					global $post;
					$args = array( 'posts_per_page' => 10, 'post_type'=> 'page', 'post_parent' => $post->ID, 'orderby' => 'menu_order', 'order' => 'asc' );
					$myposts = get_posts( $args );
					$i=1;
					foreach ( $myposts as $post ) : 
					  	setup_postdata( $post ); ?>
						<li><a class="<?=$i==1?'menu_active':''?>" href="#<?=$post->post_name?>" id="<?=$post->post_name?>_link" title="<?php the_title(); ?>"><?php the_title(); ?></a></li>
					<?php $i++; endforeach;
					wp_reset_postdata(); ?>
				</ul>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>

	<?php $the_post = get_post(44); ?>
	<div class="menu_part_image_wrapper" style="background-image: url('<?=get_thumbnail($the_post->ID, 'full')?>');" id="the-kitchen">
		<div class="menu_part_image container">
			<div class="row">
				<div class="col-sm-4 col-sm-offset-8">
					<h2 class="menu_part_title"><?=$the_post->post_title?></h2>
					<div class="menu_part_text"><?=wpautop($the_post->post_content)?></div>
				</div>
			</div>
		</div>
	</div>
	<div class="menu_details_wrapper">
		<div class="menu_details container">
			<div class="row">
				<div class="col-sm-6">
					<?php
						global $post;
						$args = array('posts_per_page' => 10, 'post_type'=> 'page', 'post_parent' => 44, 'orderby' => 'menu_order', 'order' => 'asc', 'meta_query' => array(
							array(
								'key' => 'position',
								'value' => 'left',
							)
						));
						$myposts = get_posts( $args );
						foreach ( $myposts as $post ) : 
						  	setup_postdata( $post ); ?>
							<div class="menu_detail">
								<h3 class="menu_detail_title"><?php the_title(); ?></h3>
								<div class="menu_detail_text"><?php the_content(); ?></div>
							</div>
						<?php endforeach;
						wp_reset_postdata(); ?>
				</div>
				<div class="col-sm-6">
					<?php
						global $post;
						$args = array('posts_per_page' => 10, 'post_type'=> 'page', 'post_parent' => 44, 'orderby' => 'menu_order', 'order' => 'asc', 'meta_query' => array(
							array(
								'key' => 'position',
								'value' => 'right',
							)
						));
						$myposts = get_posts( $args );
						foreach ( $myposts as $post ) : 
						  	setup_postdata( $post ); ?>
							<div class="menu_detail">
								<h3 class="menu_detail_title"><?php the_title(); ?></h3>
								<div class="menu_detail_text"><?php the_content(); ?></div>
							</div>
						<?php endforeach;
						wp_reset_postdata(); ?>
				</div>
			</div>
		</div>
	</div>

	<?php $the_post = get_post(45); ?>
	<div class="menu_part_image_wrapper" style="background-image: url('<?=get_thumbnail($the_post->ID, 'full')?>');" id="the-bar">
		<div class="menu_part_image container">
			<div class="row">
				<div class="col-sm-4">
					<h2 class="menu_part_title"><?=$the_post->post_title?></h2>
					<div class="menu_part_text"><?=wpautop($the_post->post_content)?></div>
				</div>
			</div>
		</div>
	</div>
	<div class="menu_details_wrapper">
		<div class="menu_details container">
			<div class="row">
				<div class="col-sm-6">
					<?php
						global $post;
						$args = array('posts_per_page' => 10, 'post_type'=> 'page', 'post_parent' => 45, 'orderby' => 'menu_order', 'order' => 'asc', 'meta_query' => array(
							array(
								'key' => 'position',
								'value' => 'left',
							)
						));
						$myposts = get_posts( $args );
						foreach ( $myposts as $post ) : 
						  	setup_postdata( $post ); ?>
							<div class="menu_detail">
								<h3 class="menu_detail_title"><?php the_title(); ?></h3>
								<div class="menu_detail_text"><?php the_content(); ?></div>
							</div>
						<?php endforeach;
						wp_reset_postdata(); ?>
				</div>
				<div class="col-sm-6" id="wine_div">
					<?php
						global $post;
						$args = array('posts_per_page' => 10, 'post_type'=> 'page', 'post_parent' => 45, 'orderby' => 'menu_order', 'order' => 'asc', 'meta_query' => array(
							array(
								'key' => 'position',
								'value' => 'right',
							)
						));
						$myposts = get_posts( $args );
						foreach ( $myposts as $post ) : 
						  	setup_postdata( $post ); ?>
							<div class="menu_detail">
								<h3 class="menu_detail_title"><?php the_title(); ?></h3>
								<div class="menu_detail_text"><?php the_content(); ?></div>
							</div>
						<?php endforeach;
						wp_reset_postdata(); ?>
				</div>
				<div class="col-sm-12" id="liquor_div">
					<?php
						global $post;
						$args = array('posts_per_page' => 10, 'post_type'=> 'page', 'post_parent' => 45, 'orderby' => 'menu_order', 'order' => 'asc', 'meta_query' => array(
							array(
								'key' => 'position',
								'value' => 'center',
							)
						));
						$myposts = get_posts( $args );
						foreach ( $myposts as $post ) : 
						  	setup_postdata( $post ); ?>
							<div class="menu_detail">
								<h3 class="menu_detail_title text-center"><?php the_title(); ?></h3>
								<div class="menu_detail_text"><?php the_content(); ?></div>
							</div>
						<?php endforeach;
						wp_reset_postdata(); ?>
				</div>
			</div>
		</div>
	</div>

	<?php $the_post = get_post(46); ?>
	<div class="menu_part_image_wrapper" style="background-image: url('<?=get_thumbnail($the_post->ID, 'full')?>');" id="happy-hour">
		<div class="menu_part_image container">
			<div class="row">
				<div class="col-sm-4 col-sm-offset-8">
					<h2 class="menu_part_title"><?=$the_post->post_title?></h2>
					<div class="menu_part_text"><?=wpautop($the_post->post_content)?></div>
				</div>
			</div>
		</div>
	</div>
	<div class="menu_details_wrapper">
		<div class="menu_details container">
			<div class="happy_top text-center">
				<h4>Happy Hour</h4>
				<div class="row">
					<div class="col-sm-4 col-sm-offset-4">
						<div class="happy_top_content"><?=get_post_meta(46, 'time', true)?></div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-6" id="happy_left">
					<?php
						global $post;
						$args = array('posts_per_page' => 10, 'post_type'=> 'page', 'post_parent' => 46, 'orderby' => 'menu_order', 'order' => 'asc', 'meta_query' => array(
							array(
								'key' => 'position',
								'value' => 'left',
							)
						));
						$myposts = get_posts( $args );
						foreach ( $myposts as $post ) : 
						  	setup_postdata( $post ); ?>
							<div class="menu_detail">
								<h3 class="menu_detail_title"><?php the_title(); ?></h3>
								<div class="menu_detail_text"><?php the_content(); ?></div>
							</div>
						<?php endforeach;
						wp_reset_postdata(); ?>
				</div>
				<div class="col-sm-6">
					<?php
						global $post;
						$args = array('posts_per_page' => 10, 'post_type'=> 'page', 'post_parent' => 46, 'orderby' => 'menu_order', 'order' => 'asc', 'meta_query' => array(
							array(
								'key' => 'position',
								'value' => 'right',
							)
						));
						$myposts = get_posts( $args );
						foreach ( $myposts as $post ) : 
						  	setup_postdata( $post ); ?>
							<div class="menu_detail">
								<h3 class="menu_detail_title"><?php the_title(); ?></h3>
								<div class="menu_detail_text"><?php the_content(); ?></div>
							</div>
						<?php endforeach;
						wp_reset_postdata(); ?>
				</div>
			</div>
		</div>
	</div>
<?php get_footer() ?>