<?php
	/**
	 * Template Name: Reservations
	 */
?>
<?php get_header() ?>
<?php the_post(); ?>

	<div id="wrapper" class="container">
		


		<div id="reservations_block_4">
			<div id="res_slider3">
				<script type="text/javascript">var submitted3 = false;</script>
				<iframe id="hidden_iframe3" onload="if(submitted3) { jQuery('#message_sent').fadeIn(); jQuery('#ss-form3')[0].reset(); booking_slider2.goToSlide(1); }" style="display:none;" name="hidden_iframe3"></iframe>
				<form id="ss-form3" onsubmit="submitted3=true;" target="hidden_iframe3" method="POST" action="<?=home_url()?>/wp-content/plugins/fulton/ajax.php?reservation2=1" novalidate="novalidate">
				<?php /*<form id="ss-form3" method="POST" action="<?=home_url()?>/wp-content/plugins/fulton/ajax.php?reservation2=1" novalidate="novalidate">*/ ?>
					<ul class="bxslider">
						<?php /*<li>
							<div class="col-sm-7">
								<h2 class="groups_title"><?=get_post_meta($post->ID, 'block_4_title', true)?></h2>
								<div class="groups_text"><?=wpautop(get_post_meta($post->ID, 'block_4_text', true))?></div>
							</div>
							<div class="col-sm-4 col-sm-offset-1">
								<div id="event_reg_panel">
									<h3>Want to reserve a lane?</h3>
									<p>
										Lane-Only Reservations:<br>
										$25 per person
									</p>
									<a href="javascript:void(0);" title="Reserve a Lane" id="booking_inquiry5">Reserve a Lane</a>
								</div>
							</div>
						</li>*/ ?>
						<li>
							<h3 class="ef_title"><?=get_post_meta($post->ID, 'lane_reservation_title', true)?></h3>
							<div class="ef_text"><?=wpautop(get_post_meta($post->ID, 'lane_reservation_text', true))?></div>
							<div class="row ef_row">
								<div class="col-sm-3">
									<label>First Name*</label>
									<input type="text" aria-invalid="false" aria-required="true" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" size="40" value="" id="firstname2" name="firstname2">
								</div>
								<div class="col-sm-3">
									<label>Last Name*</label>
									<input type="text" aria-invalid="false" aria-required="true" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" size="40" value="" name="lastname2" id="lastname2">
								</div>
								<div class="col-sm-3">
									<label>Email Address*</label>
									<input type="email" aria-invalid="false" aria-required="true" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" size="40" value="" name="youremail2" id="youremail2">
								</div>
								<div class="col-sm-3">
									<label>Phone Number*</label>
									<input type="text" aria-invalid="false" aria-required="true" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" size="40" value="" name="phone2" id="phone2">
								</div>
							</div>
							<div class="row ef_row">
								<div class="col-sm-3">
									<div class="row">
										<div class="col-sm-6">
											<label>Date*</label>
											<?php /*<input type="text" aria-invalid="false" aria-required="true" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" size="40" value="" name="lane_date" id="lane_date">*/ ?>
											<div class="select_dropdown">
												<select aria-invalid="false" class="wpcf7-form-control wpcf7-select" name="lane_date" id="lane_date">
													<option value=""></option>
													<option value="<?=date('m/d/Y')?>"><?=date('D, M j')?></option>
													<?php $current_week = date('W'); ?>
													<?php for ($i=1; $i<=6; $i++) {
														$week = date('W', strtotime('+'.$i.' days'));
														if ($week == $current_week) { ?>
															<option value="<?=date('m/d/Y', strtotime('+'.$i.' day'))?>"><?=date('D, M j', strtotime('+'.$i.' day'))?></option>
														<?php } ?>
													<?php } ?>
													<?php /*for ($i=2; $i<=10; $i++) { ?>
														<option value="<?=$i?> Hours"><?=$i?> Hours</option>
													<?php }*/ ?>
												</select>
											</div>
										</div>
										<div class="col-sm-6">
											<label>Preferred Time*</label>
											<?php /*<input type="text" aria-invalid="false" class="wpcf7-form-control wpcf7-text" size="40" value="" name="lane_time" id="lane_time">*/ ?>
											<div class="select_dropdown">
												<select aria-invalid="false" class="wpcf7-form-control wpcf7-select" name="lane_time" id="lane_time">
													<option value=""></option>
													<option value="12:00am">12:00am</option>
													<option value="12:30am">12:30am</option>
													<option value="1:00am">1:00am</option>
													<option value="1:30am">1:30am</option>
													<option value="2:00am">2:00am</option>
													<option value="2:30am">2:30am</option>
													<option value="3:00am">3:00am</option>
													<option value="3:30am">3:30am</option>
													<option value="4:00am">4:00am</option>
													<option value="4:30am">4:30am</option>
													<option value="5:00am">5:00am</option>
													<option value="5:30am">5:30am</option>
													<option value="6:00am">6:00am</option>
													<option value="6:30am">6:30am</option>
													<option value="7:00am">7:00am</option>
													<option value="7:30am">7:30am</option>
													<option value="8:00am">8:00am</option>
													<option value="8:30am">8:30am</option>
													<option value="9:00am">9:00am</option>
													<option value="9:30am">9:30am</option>
													<option value="10:00am">10:00am</option>
													<option value="10:30am">10:30am</option>
													<option value="11:00am">11:00am</option>
													<option value="11:30am">11:30am</option>
													<option value="12:00pm">12:00pm</option>
													<option value="12:30pm">12:30pm</option>
													<option value="1:00pm">1:00pm</option>
													<option value="1:30pm">1:30pm</option>
													<option value="2:00pm">2:00pm</option>
													<option value="2:30pm">2:30pm</option>
													<option value="3:00pm">3:00pm</option>
													<option value="3:30pm">3:30pm</option>
													<option value="4:00pm">4:00pm</option>
													<option value="4:30pm">4:30pm</option>
													<option value="5:00pm">5:00pm</option>
													<option value="5:30pm">5:30pm</option>
													<option value="6:00pm">6:00pm</option>
													<option value="6:30pm">6:30pm</option>
													<option value="7:00pm">7:00pm</option>
													<option value="7:30pm">7:30pm</option>
													<option value="8:00pm">8:00pm</option>
													<option value="8:30pm">8:30pm</option>
													<option value="9:00pm">9:00pm</option>
													<option value="9:30pm">9:30pm</option>
													<option value="10:00pm">10:00pm</option>
													<option value="10:30pm">10:30pm</option>
													<option value="11:00pm">11:00pm</option>
													<option value="11:30pm">11:30pm</option>
												</select>
											</div>
										</div>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="row">
										<div class="col-sm-6">
											<label>Number of Guests*</label>
											<div class="select_dropdown">
												<select aria-invalid="false" class="wpcf7-form-control wpcf7-select" name="lane_guests" id="lane_guests">
													<option value=""></option>
													<?php /*<option value="1">1 Guest</option>*/ ?>
													<?php for ($i=6; $i<=18; $i++) { ?>
														<option value="<?=$i?>"><?=$i?> Guests</option>
													<?php } ?>
												</select>
											</div>
										</div>
										<div class="col-sm-6">
											<label>Number of Lanes*</label>
											<div class="select_dropdown">
												<select aria-invalid="false" class="wpcf7-form-control wpcf7-select" name="lane_number" id="lane_number">
													<option value=""></option>
													<option value="1 Lane">1 Lane</option>
													<option value="2 Lane">2 Lanes</option>
													<?php /*for ($i=1; $i<=10; $i++) { ?>
														<option value="<?=$i?> Lanes"><?=$i?> Lanes</option>
													<?php }*/ ?>
												</select>
											</div>
										</div>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="row">
										<div class="col-sm-3">
											<label>Number of Hours*</label>
											<div class="select_dropdown">
												<select aria-invalid="false" class="wpcf7-form-control wpcf7-select" name="lane_hours" id="lane_hours">
													<option value=""></option>
													<option value="1 Hour">1 Hour</option>
													<option value="1.5 Hours">1.5 Hours</option>
													<option value="2 Hours">2 Hours</option>
													<?php /*for ($i=2; $i<=10; $i++) { ?>
														<option value="<?=$i?> Hours"><?=$i?> Hours</option>
													<?php }*/ ?>
												</select>
											</div>
										</div>
										<div class="col-sm-6">
											<label>Interested in adding a food or beverage package?*</label>
											<div class="select_dropdown" id="food_dropdown_wrapper">
												<select aria-invalid="false" class="wpcf7-form-control wpcf7-select" name="lane_food" id="lane_food">
													<option value=""></option>
													<option value="Yes">Yes</option>
													<option value="No">No</option>
													<?php /*for ($i=2; $i<=10; $i++) { ?>
														<option value="<?=$i?> Hours"><?=$i?> Hours</option>
													<?php }*/ ?>
												</select>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row ef_row">
								<div class="col-sm-12">
									<div class="reserve_line_text ef_text">
										<?=wpautop(get_post_meta($post->ID, 'lane_reservation_bottom_text', true))?>
									</div>
								</div>
							</div>
							<div class="row ef_row">
								<div class="col-sm-9">
									<input type="submit" class="wpcf7-form-control wpcf7-submit" value="Submit Inquiry">
									<div id="circularG" class="ajax_loader">
										<div id="circularG_1" class="circularG"></div>
										<div id="circularG_2" class="circularG"></div>
										<div id="circularG_3" class="circularG"></div>
										<div id="circularG_4" class="circularG"></div>
										<div id="circularG_5" class="circularG"></div>
										<div id="circularG_6" class="circularG"></div>
										<div id="circularG_7" class="circularG"></div>
										<div id="circularG_8" class="circularG"></div>
									</div>
								</div>
								<div class="col-sm-3 text-right required_label">*Required fields</div>
							</div>
						</li>
						<li>
							<div class="col-sm-12">
								<h2 class="groups_title">Thank You For your Lane Reservation Inquiry</h2>
								<div class="groups_text"><p>A Fulton Alley team member will be in contact shortly to confirm the your lane reservation request.</p></div>
							</div>
							<?php /*<div class="col-sm-4 col-sm-offset-1">
								<div id="event_reg_panel">
									<h3>Want to reserve a lane?</h3>
									<p>
										Lane Only Reservations:<br>
										$25 per person
									</p>
									<a href="javascript:void(0);" title="Reserve a Lane" id="booking_inquiry6">Reserve a Lane</a>
								</div>
							</div>*/ ?>
						</li>
					</ul>
				</form>
			</div>
		</div>
	</div>
<?php get_footer() ?>