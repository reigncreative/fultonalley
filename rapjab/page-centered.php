<?php
	/**
	 * Template Name: Centered Page
	 */
?>

<?php get_header() ?>
	<div id="wrapper" class="container text-center centered_page">
		<div class="row">
			<div id="container" class="col-sm-12">
	            <?php the_post() ?>
				<div id="post-<?php the_ID() ?>" <?php post_class(); ?>>
					<h2 class="entry-title normal_page_title"><?php the_title() ?></h2>
					<div class="entry-content normal_page_content">
	                    <?php the_content() ?>
					</div>
				</div><!-- .post -->
			</div><!-- #container -->
			<?php //get_sidebar() ?>
		</div>
	</div><!-- #wrapper -->
<?php get_footer() ?>