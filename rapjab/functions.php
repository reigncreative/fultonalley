<?php
    /*
     *RapJab
     */


add_theme_support( 'post-thumbnails' );
/**
 * Thumbnails
 */
add_image_size('event_thumb', 263, 263, true);

/**
 * Excerpt
 */
function custom_excerpt_length( $length ) {
	return 20;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

function new_excerpt_more( $more ) {
	return '...';
}
add_filter('excerpt_more', 'new_excerpt_more');

/*function new_excerpt_more( $more ) {
	return ' <a class="read-more" href="' . get_permalink( get_the_ID() ) . '">' . __( 'Read More', 'your-text-domain' ) . '</a>';
}
add_filter( 'excerpt_more', 'new_excerpt_more' );*/


/**
 * Text cutter.
 */
function cuttext($text,$hossz){
    if (strlen($text)>$hossz) {
        $text = strip_tags($text);
        $text = urldecode($text);
        $tmp = explode(" ",$text);
        $ret = "";
        foreach($tmp as $k=>$v){
            $next = $ret." ".$v;
            if(strlen($next)<=$hossz){
                $ret.= " ".$v;
            }else{
                return $ret."...";
            }
        }
        return $ret."...";
    } else {
        return $text;
    }   
}

/**
 * Returns the featured image the post, or the no picture image.
 */
function get_thumbnail($post_id, $size) {
    $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id($post_id), $size);
    if ($large_image_url[0] != "") {
        $picture = $large_image_url[0];
    } else {
        $picture = FALSE;
    }
    return $picture;
}

/**
 * Options admin menu
 */
add_action('admin_menu', 'register_custom_menu');
function register_custom_menu() {
   add_menu_page('Options', 'Options', 'add_users', 'options', 'admin_menu', '');
}

function admin_menu() {

    if ($_POST['submit']) {
        update_option('phone_number', $_POST['phone_number']);
        update_option('email', $_POST['email']);
        update_option('phone_number2', $_POST['phone_number2']);
        update_option('email2', $_POST['email2']);
        update_option('fb_url', $_POST['fb_url']);
        update_option('tw_url', $_POST['tw_url']);
        update_option('pt_url', $_POST['pt_url']);
        update_option('ig_url', $_POST['ig_url']);
        update_option('address', $_POST['address']);
    }

    $phone_number = get_option('phone_number');
    $email = get_option('email');
    $phone_number2 = get_option('phone_number2');
    $email2 = get_option('email2');
    $fb_url = get_option('fb_url');
    $tw_url = get_option('tw_url');
    $pt_url = get_option('pt_url');
    $ig_url = get_option('ig_url');
    $address = get_option('address');
    ?>
    <div class="admin_page">
            <div id="admin_nav">
                <span class="admin-menupont" id="menu_uj_meret">Options</span>
            </div>
            <div id="meretek" class="admin_content">
                <div id="uj_meret" class="admin-item">
                    <h2 class="oldalcim">Options</h2>
                    <div>
                        <form action="" method="post">
                            <p>General Phone Number: <input type="text" name="phone_number" value="<?=$phone_number?>"></p>
                            <p>General Email: <input type="text" name="email" value="<?=$email?>"></p>
                            <p>Event Phone Number: <input type="text" name="phone_number2" value="<?=$phone_number2?>"></p>
                            <p>Event Email: <input type="text" name="email2" value="<?=$email2?>"></p>
                            <p>Address: <input type="text" name="address" value="<?=$address?>"></p>
                            <p>Facebook URL: <input type="text" name="fb_url" value="<?=$fb_url?>"></p>
                            <p>Twitter URL: <input type="text" name="tw_url" value="<?=$tw_url?>"></p>
                            <p>Pinterest URL: <input type="text" name="pt_url" value="<?=$pt_url?>"></p>
                            <p>Instagram URL: <input type="text" name="ig_url" value="<?=$ig_url?>"></p>
                            <p><input type="submit" name="submit" value="Save" title="Save" class="button-primary"></p>
                        </form>
                    </div>
                </div>
                
            </div>
            <div class="clear"></div>
        </div>
<?php }

register_sidebar( array(
    'name' => __( 'Sidebar Widgets' ),
    'id' => 'sidebar-1',
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget' => "</aside>",
    'before_title' => '<h3 class="widget-title">',
    'after_title' => '</h3>',
) );

register_sidebar( array(
    'name' => __( 'Footer Widgets' ),
    'id' => 'footer-1',
    'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget' => "</div>",
    'before_title' => '<h3 class="widget-title">',
    'after_title' => '</h3>',
) );

register_sidebar( array(
    'name' => __( 'Events Footer Widgets' ),
    'id' => 'footer-events-1',
    'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget' => "</div>",
    'before_title' => '<h3 class="widget-title">',
    'after_title' => '</h3>',
) );

function get_months() {
    $months = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
    return $months;
}
remove_action('wp_head', 'wp_generator');
