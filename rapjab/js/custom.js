var booking_slider;

jQuery(document).ready(function(){
	var iframe = jQuery('#vimeo-player')[0];
	var player = $f(iframe);

	/*jQuery('#close_broll').click(function() {
	    //alert('stoped');
	    
	});*/

	$('#event_popup_close').click(function(){
		$('#event_popup').addClass('hidden');
	});

	player.addEvent('ready', function() {
        
        player.addEvent('finish', onFinish);
    });

    function onFinish(id) {
        jQuery('#broll').stop().animate({opacity: 0}, 500, function(){
			jQuery('#broll').css('z-index', '-1');
		});
		jQuery('#video_button').removeClass('active_home_button');
		jQuery('#tour_button').removeClass('active_home_button');
		jQuery('#home_header_buttons').show();
		jQuery('#video_button').html('Play Video');
		jQuery('#video_button').attr('title', 'Play Video');
    }

	jQuery('#close_broll').click(function(){
		jQuery('#broll').stop().animate({opacity: 0}, 500, function(){
			jQuery('#broll').css('z-index', '-1');
		});
		jQuery('#video_button').removeClass('active_home_button');
		jQuery('#tour_button').removeClass('active_home_button');
		jQuery('#home_header_buttons').show();
		jQuery('#video_button').html('Play Video');
		jQuery('#video_button').attr('title', 'Play Video');
		player.api('pause');
		//jQuery('#broll iframe').attr('src', jQuery('#broll iframe').attr('src'));
	});


	jQuery('#video_button').click(function(){
	    //alert('play');
	    player2.api('play');
	});

	var iframe2 = jQuery('#vimeo-groups_video')[0];
	var player2 = $f(iframe2);

	/*jQuery('#close_broll').click(function() {
	    //alert('stoped');
	    
	});*/

	player2.addEvent('ready', function() {
        
        player2.addEvent('finish', onFinish2);
    });

    function onFinish2() {
        jQuery('#groups_video').stop().animate({opacity: 0}, 500, function(){
			jQuery('#groups_video').css('z-index', '-1');
		});
		/*jQuery('#video_button').removeClass('active_home_button');
		jQuery('#video_button').html('Play Video');
		jQuery('#video_button').attr('title', 'Play Video');*/
    }

	jQuery('#close_groups_video').click(function(){
		jQuery('#groups_video').stop().animate({opacity: 0}, 500, function(){
			jQuery('#groups_video').css('z-index', '-1');
		});
		/*jQuery('#video_button').removeClass('active_home_button');
		jQuery('#tour_button').removeClass('active_home_button');
		jQuery('#home_header_buttons').show();
		jQuery('#video_button').html('Play Video');
		jQuery('#video_button').attr('title', 'Play Video');*/
		player2.api('pause');
		//jQuery('#broll iframe').attr('src', jQuery('#broll iframe').attr('src'));
	});


	jQuery('#video_button_groups').click(function(){
	    jQuery('#groups_video').css('z-index', '300000');
		jQuery('#groups_video').stop().animate({opacity: 1}, 500);
	    player2.api('play');
	});

	if (jQuery('#home_video').length) {
		var iterations = 1;
		//document.getElementById('iteration').innerText = iterations;
		home_video.addEventListener('ended', function () {    
		    if (iterations < 3) {   
		        this.currentTime = 0;
		        this.play();
		        iterations ++;
		        //document.getElementById('iteration').innerText = iterations;
		    }   

		}, false);
	}

	if (jQuery('#home_video').length) {
		var iterations = 1;
		//document.getElementById('iteration').innerText = iterations;
		home_video.addEventListener('ended', function () {    
		    if (iterations < 3) {   
		        this.currentTime = 0;
		        this.play();
		        iterations ++;
		        //document.getElementById('iteration').innerText = iterations;
		    }   

		}, false);
	}

	if (jQuery('#top_slider').length) {
		jQuery('.bxslider').bxSlider({
			auto: 7000,
			speed: 1000
		});
	}

	if (jQuery('#be_slider').length) {
		jQuery('#be_slider .bxslider').bxSlider({
			auto: 7000,
			speed: 1000,
			pager: false
		});
	}

	if (jQuery('#reservations_block_2').length) {
		reser_top_slide = jQuery('#reservations_block_2 .bxslider').bxSlider({
			speed: 1000,
			//controls: false,
			//pager: false,
			pagerCustom: '#bx-pager',
			adaptiveHeight: true
		});

		jQuery('.gotoslide_link').click(function(){
			slide_to = jQuery(this).attr('data-slide');
			reser_top_slide.goToSlide(slide_to);
		});
	}
	//var booking_slider
	if (jQuery('#reservations_block_3').length) {
		booking_slider = jQuery('#reservations_block_3 .bxslider').bxSlider({
			speed: 1000,
			controls: false,
			pager: false,
			adaptiveHeight: true,
			infiniteLoop: false,
			touchEnabled: false
		});

		jQuery('#booking_inquiry').click(function(){
			booking_slider.goToSlide(1);
		});

		jQuery('#booking_inquiry2').click(function(){
			booking_slider.goToSlide(1);
		});
	}

	$('select[name=how_hear]').on('change', function(e){
        var optionSelected = $(this).val();
        if (optionSelected == "Other") {
        	jQuery('.hide_other').show();
        } else {
        	jQuery('.hide_other').hide();
        	jQuery('.hide_other input').val('');
        }
    });

	if (jQuery('#reservations_block_4').length) {
		booking_slider2 = jQuery('#reservations_block_4 .bxslider').bxSlider({
			speed: 1000,
			controls: false,
			pager: false,
			adaptiveHeight: true,
			infiniteLoop: false,
			touchEnabled: false
		});

		jQuery('#booking_inquiry5').click(function(){
			booking_slider2.goToSlide(1);
		});

		jQuery('#booking_inquiry6').click(function(){
			booking_slider2.goToSlide(1);
		});
	}

	setInterval(function(){
		if (jQuery(window).width() < 768) {
			jQuery('.hb_form input[type="submit"]').val('Sign-Up');
		} else {
			jQuery('.hb_form input[type="submit"]').val('Sign-Up for Email');
		}
	}, 2000);

	jQuery('#toggle_mobile_nav').click(function(){
		if (jQuery(this).hasClass('mobile_menu_open')) {
			jQuery(this).removeClass('mobile_menu_open');
			jQuery('#mobile_menu_wrapper').slideUp();
		} else {
			jQuery(this).addClass('mobile_menu_open');
			jQuery('#mobile_menu_wrapper').slideDown();
		}
	});
	
	if (jQuery('#game_1').length) {
		jQuery('.bxslider').bxSlider({
			auto: 7000,
			speed: 1000,
			pager: false
		});
	}

	if (jQuery('#twitter_slider').length) {
		jQuery('.bxslider').bxSlider({
			controls: false
		});
	}

	jQuery('#tour_button').click(function(){
		jQuery('#google_360').css('z-index', '30');
		jQuery('#google_360').stop().animate({opacity: 1}, 500);
		jQuery('#tour_button').addClass('active_home_button');
		jQuery('#video_button').removeClass('active_home_button');
		jQuery('#broll').stop().animate({opacity: 0}, 500, function(){
			jQuery('#broll').css('z-index', '-1');
		});
		jQuery('#video_button').html('Play Video');
		jQuery('#video_button').attr('title', 'Play Video');
		jQuery('#broll iframe').attr('src', jQuery('#broll iframe').attr('src'));

		if (jQuery('#google_360').css('opacity') == 1) {
			jQuery('#tour_button').html('Take a Tour');
			jQuery('#tour_button').attr('title', 'Take a Tour');
			jQuery('#google_360').stop().animate({opacity: 0}, 500, function(){
				jQuery('#google_360').css('z-index', '-1');
			});
			jQuery('#tour_button').removeClass('active_home_button');
			//jQuery('#video_button').addClass('active_home_button');
		} else {
			
			jQuery('#tour_button').html('X Close Tour');
			jQuery('#tour_button').attr('title', 'X Close Tour');
			jQuery('#google_360').css('z-index', '30');
			jQuery('#google_360').stop().animate({opacity: 1}, 500);
			jQuery('#tour_button').addClass('active_home_button');
			jQuery('#video_button').removeClass('active_home_button');
		}
	});
	


	jQuery('#video_button').click(function(){
		jQuery('#broll').css('z-index', '30');
		jQuery('#broll').stop().animate({opacity: 1}, 500);
		jQuery('#video_button').addClass('active_home_button');
		jQuery('#tour_button').removeClass('active_home_button');
		jQuery('#google_360').stop().animate({opacity: 0}, 500, function(){
			jQuery('#google_360').css('z-index', '-1');
		});
		jQuery('#tour_button').html('Take a Tour');
		jQuery('#tour_button').attr('title', 'Take a Tour');
		jQuery('#home_header_buttons').hide();
		player.api('play');

		if (jQuery('#broll').css('opacity') == 1) {
			jQuery('#video_button').html('Play Video');
			jQuery('#video_button').attr('title', 'Play Video');
			jQuery('#broll').stop().animate({opacity: 0}, 500, function(){
				jQuery('#broll').css('z-index', '-1');
			});
			jQuery('#video_button').removeClass('active_home_button');
			//jQuery('#tour_button').addClass('active_home_button');
			//jQuery('iframe').each(function(){
			jQuery('#broll iframe').attr('src', jQuery('#broll iframe').attr('src'));
			//});
		} else {
			
			jQuery('#video_button').html('X Close Video');
			jQuery('#video_button').attr('title', 'X Close Video');
			jQuery('#broll').css('z-index', '30');
			jQuery('#broll').stop().animate({opacity: 1}, 500);
			jQuery('#video_button').addClass('active_home_button');
			jQuery('#tour_button').removeClass('active_home_button');
		}
	});

	jQuery('#menu_nav_inner a').click(function(e) {
		e.preventDefault(); 
	    goToByScroll(jQuery(this).attr("href"));
	});

	jQuery('#booking_an_event_link').click(function(){
		goToByScroll('#reservations_block_3');
	});

	jQuery('#footer .widget_footer_contact').addClass('col-sm-offset-1');

	jQuery('#learn_more').click(function(){
		jQuery('html,body').animate({
	        scrollTop: jQuery('#wrapper').offset().top},
        'slow');
	});

	helper_height = jQuery('#helper_div').outerHeight();
	jQuery('#header-home-wrap').css('height', helper_height+'px');

	if (jQuery('#google_map').length) {
		initialize();
	}

	/*jQuery('#better_events_form form').submit(function(e){
		
		var data = { 
		 lead: {
		  first_name: jQuery('input[name=firstname]').val(),
		  last_name: jQuery('input[name=lastname]').val(),
		  phone_number: jQuery('input[name=phone]').val(),
		  email_address: jQuery('input[name=youremail]').val(),
		  event_description: jQuery('select[name=event]').val(),
		  company: jQuery('input[name=company]').val(),
		  event_date: jQuery('input[name=event_date]').val(),
		  start_time: jQuery('input[name=start_time]').val(),
		  end_time: jQuery('input[name=end_time]').val(),
		  guest_count: jQuery('input[name=attendees]').val(),
		  additional_information: jQuery('input[name=yourmessage]').val(),
		 },
		 lead_form_id: 123
		}

		jQuery.ajax({data: data, 
		 //url: 'http://api.tripleseat.com/v1/leads/create.js?public_key=asdf',
		 url: '/v1/leads/create.js?public_key=asdf',
		 dataType:'JSONP', 
		 //crossDomain:true, 
		 success: function(data) { 
		   if (data.errors != undefined) {
		     // handle errors
		   } else {
		     // show data.success_message
		   }
		 } 
		});
        e.preventDefault();
	});*/
	

	if (jQuery("#better_events_form form").length) {
		jQuery("#better_events_form form").validate({
			rules: {
				firstname: "required",
				lastname: "required",
				phone: "required",
				youremail: {
					required: true,
					email: true
				},
				zip: {
					required: true,
					minlength: 5,
					number: true
				},
				event_date: "required"
			},
			messages: {
				firstname: "Please enter your firstname",
				lastname: "Please enter your lastname",
				phone: "Please enter your phone number",
				youremail: "Please enter a valid email address",
				zip: "Please enter a valid zip code",
				event_date: "Please enter the event's date"
			},
			/*submitHandler: function(e) {
				var data = { 
				 lead: {
				  first_name: jQuery('input[name=firstname]').val(),
				  last_name: jQuery('input[name=lastname]').val(),
				  phone_number: jQuery('input[name=phone]').val(),
				  email_address: jQuery('input[name=youremail]').val(),
				  event_description: jQuery('select[name=event]').val(),
				  company: jQuery('input[name=company]').val(),
				  event_date: jQuery('input[name=event_date]').val(),
				  start_time: jQuery('input[name=start_time]').val(),
				  end_time: jQuery('input[name=end_time]').val(),
				  guest_count: jQuery('input[name=attendees]').val(),
				  additional_information: jQuery('input[name=yourmessage]').val(),
				 },
				 lead_form_id: 123
				}

				jQuery.ajax({data: data, 
				 //url: 'http://api.tripleseat.com/v1/leads/create.js?public_key=asdf',
				 url: '/v1/leads/create.js?public_key=asdf',
				 dataType:'JSONP', 
				 //crossDomain:true, 
				 success: function(data) { 
				   if (data.errors != undefined) {
				     // handle errors
				   } else {
				     // show data.success_message
				   }
				 } 
				});
		        e.preventDefault();
			}*/
		});
	}

	if (jQuery("#reservations_block_3 form").length) {
		jQuery( "input[name=event_date]" ).datepicker({
			inline: true
		});

		jQuery("#reservations_block_3 form").validate({
			rules: {
				firstname: "required",
				lastname: "required",
				phone: "required",
				event: "required",
				start_time: "required",
				end_time: "required",
				attendees: {
					required: true,
					number: true,
					min: 6
				},
				youremail: {
					required: true,
					email: true
				},
				event_date: "required",
				how_hear: "required",
			},
			messages: {
				firstname: "Please enter your firstname",
				lastname: "Please enter your lastname",
				phone: "Please enter your phone number",
				youremail: "Please enter a valid email address",
				event_date: "Please enter the event's date"
			},
		});

		setInterval(function(){
			jQuery('.select_dropdown').each(function(){
				if (jQuery(this).find('select').hasClass('error')) {
					jQuery(this).css('border-color', '#ed3636');
				} else {
					jQuery(this).css('border-color', '#b48e66');
				}
			});
		}, 1000);
	}

	if (jQuery("#reservations_block_4 form").length) {
		jQuery( "input[name=lane_date]" ).datepicker({
			inline: true
		});

		jQuery("#reservations_block_4 form").validate({
			rules: {
				firstname2: "required",
				lastname2: "required",
				phone2: "required",
				lane_date: "required",
				lane_time: "required",
				end_time: "required",
				lane_guests: "required",
				youremail2: {
					required: true,
					email: true
				},
				lane_number: "required",
				lane_hours: "required",
				lane_food: "required"
			}
			
		});

		setInterval(function(){
			jQuery('#reservations_block_4 .select_dropdown').each(function(){
				if (jQuery(this).find('select').hasClass('error')) {
					jQuery(this).css('border-color', '#ed3636');
				} else {
					jQuery(this).css('border-color', '#b48e66');
				}
			});
		}, 1000);
	}

	jQuery('#reserve_event_book').click(function() {
	    goToByScroll2('#reservations_block_3');
	});

	jQuery('#reserve_lane_book').click(function() {
	    goToByScroll2('#reservations_block_4');
	});

	jQuery('#another_book_an_event_link').click(function(e) {
	    goToByScroll2('#reservations_block_3');
	    e.preventDefault();
	});
});

jQuery(window).load(function() {
	var $container = jQuery('#portfolio_ul');

	$container.isotope({
		itemSelector : '.portfolio_item'
	});

	var $optionSets = jQuery('#gallery_category_list .option-set'),
	      $optionLinks = $optionSets.find('a');

	  $optionLinks.click(function(){
	    var $this = jQuery(this);
	    // don't proceed if already selected
	    if ( $this.hasClass('selected') ) {
	      return false;
	    }
	    var $optionSet = $this.parents('.option-set');
	    $optionSet.find('.selected').removeClass('selected');
	    $this.addClass('selected');

	    // make option object dynamically, i.e. { filter: '.my-filter-class' }
	    var options = {}, 
	        key = $optionSet.attr('data-option-key'),
	        value = $this.attr('data-option-value');
	    // parse 'false' as false boolean
	    value = value === 'false' ? false : value;
	    options[ key ] = value;
	    if ( key === 'layoutMode' && typeof changeLayoutMode === 'function' ) {
	      // changes in layout modes need extra logic
	      changeLayoutMode( $this, options )
	    } else {
	      // otherwise, apply new options
	      $container.isotope( options );
	    }
	    
	    return false;
	  });

	  if (jQuery('#the-kitchen').length) {
		//alert(jQuery('#menu_nav_wrapper').offset().top - jQuery('#header-wrap').outerHeight());
		target_height = jQuery('#menu_nav_wrapper').offset().top - jQuery('#header-wrap').outerHeight();
		sticky_height = jQuery('#menu_nav_wrapper').outerHeight();

		kitchen_height = jQuery('#the-kitchen').offset().top - 150;
		bar_height = jQuery('#the-bar').offset().top - 150;
		happy_height = jQuery('#happy-hour').offset().top - 150;

	    jQuery(document).scroll(function(){
	        var top_distance = jQuery(window).scrollTop();

	        //alert(box2_distance);
	        if (top_distance >= target_height) {
	            //jQuery("#header_body").animate({opacity: 1}, 300);
	            jQuery("#menu_nav_wrapper").addClass('sticky');

	            jQuery('#the-kitchen').css('margin-top', sticky_height+'px');
	        } else {
	            //jQuery("#header_body").animate({opacity: 0}, 300);
	            jQuery("#menu_nav_wrapper").removeClass('sticky');
	            jQuery('#the-kitchen').css('margin-top', '0');
	        }

	        if (top_distance < bar_height) {
	        	jQuery('#menu_nav_inner a').removeClass('menu_active');
	        	jQuery('#the-kitchen_link').addClass('menu_active');
	        } else if ((top_distance >= bar_height) && (top_distance < happy_height)) {
	        	jQuery('#menu_nav_inner a').removeClass('menu_active');
	        	jQuery('#the-bar_link').addClass('menu_active');
	        } else if (top_distance >= happy_height) {
	        	jQuery('#menu_nav_inner a').removeClass('menu_active');
	    		jQuery('#happy-hour_link').addClass('menu_active');
	    	}
	    });
	  }

});

function goToByScroll(id){
      // Remove "link" from the ID
    id = id.replace("link", "");
      // Scroll
    jQuery('html,body').animate({
        scrollTop: jQuery(id).offset().top-(sticky_height = jQuery('#menu_nav_wrapper').outerHeight())-(sticky_height = jQuery('#header-wrap').outerHeight())},
        'slow');
}

function goToByScroll2(id){
      // Remove "link" from the ID
    id = id.replace("link", "");
      // Scroll
    jQuery('html,body').animate({
        scrollTop: jQuery(id).offset().top-jQuery('#header-wrap').outerHeight() -50},
        'slow');
}

function initialize() {
	var latlng = new google.maps.LatLng(29.946976, -90.065132);
	var stylez = [{
	      featureType: "all",
	      elementType: "all",
	      stylers: [
	        { saturation: -100 }
	      ]
	}];
	var isDraggable = jQuery(document).width() > 480 ? true : false;
	var settings = {
		zoom: 17,
		center: latlng,
		mapTypeControl: false,
		//draggable: false,
		mapTypeControlOptions: {
	         mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'grayscale']
	    },
		navigationControl: false,
		//navigationControlOptions: {style: google.maps.NavigationControlStyle.SMALL},
		zoomControl: false,
		scrollwheel: false,
        streetViewControl: false,
        draggable: isDraggable
	};
	var map = new google.maps.Map(document.getElementById("google_map"), settings);
	var mapType = new google.maps.StyledMapType(stylez, { name:"Grayscale" });    
	map.mapTypes.set('grayscale', mapType);
	map.setMapTypeId('grayscale');

	var companyImage = new google.maps.MarkerImage('/wp-content/themes/rapjab/images/map_marker.png',
		new google.maps.Size(24,27),
		new google.maps.Point(0,0)
		//new google.maps.Point(23,23)
	);

	/*var companyShadow = new google.maps.MarkerImage('images/marker_shadow.png',
		new google.maps.Size(65,97),
		new google.maps.Point(0,0),
		new google.maps.Point(18,0));*/

	var companyPos = new google.maps.LatLng(29.946976, -90.065132);

	var companyMarker = new google.maps.Marker({
		position: companyPos,
		map: map,
		icon: companyImage,
		//shadow: companyShadow,
		title:"Wayside House",
		zIndex: 3});
}