<?php get_header() ?>

	<div id="wrapper">
        <div id="container" class="col-md-8">
    		<div id="content">

            <?php if ( have_posts() ) : ?>

        		<h2 class="page-title">Search Results for: <span><?php the_search_query() ?></span></h2>
        
                <?php while ( have_posts() ) : the_post() ?>

        			<div id="post-<?php the_ID() ?>" <?php post_class(); ?>>
                        <h3 class="entry-title"><a href="<?php the_permalink() ?>" title="<?php the_title(); ?>"><?php the_title() ?></a></h3>
                        <div class="entry-date"><span class="published" title="<?php the_time('Y-m-d\TH:i:sO') ?>"><?php the_time('Y-m-d\TH:i:sO') ?></span></div>
                        <div class="entry-content">
                            <?php the_excerpt(); ?>
                        </div>
                        <div class="entry-meta">
                            <span class="author vcard"><?php printf(__('By %s'), '<a class="url fn n" href="' . get_author_link( false, $authordata->ID, $authordata->user_nicename ) . '" title="' . sprintf(__( 'View all posts by %s'), $authordata->display_name ) . '">' . get_the_author() . '</a>' ) ?></span>
                            <span class="meta-sep">|</span>
                            <span class="cat-links"><?=get_the_term_list( $post->ID, 'category', '', ', ' )?></span>
                            <span class="meta-sep">|</span>
                            <?php the_tags(__('<span class="tag-links">Tagged '), ", ", "</span>\n\t\t\t\t\t<span class=\"meta-sep\">|</span>\n") ?>
                            <span class="comments-link"><?php comments_popup_link(__('Comments (0)'), __('Comments (1)'), __('Comments (%)')) ?></span>
                        </div>
                    </div><!-- .post -->

                <?php endwhile; ?>

    			<div id="nav-below" class="navigation">
                    <div class="nav-previous"><?php next_posts_link(__('<span class="meta-nav">&laquo;</span> Older posts')) ?></div>
                    <div class="nav-next"><?php previous_posts_link(__('Newer posts <span class="meta-nav">&raquo;</span>')) ?></div>
                </div>

            <?php else : ?>

    			<div id="post-0" class="post no-results not-found">
    				<h2 class="entry-title">Nothing Found</h2>
    				<div class="entry-content">
    					<p>Sorry, but nothing matched your search criteria. Please try again with some different keywords.</p>
    				</div>
    			</div><!-- .post -->

            <?php endif; ?>

    		</div><!-- #content -->
    	</div><!-- #container -->
    </div><!-- #wrapper -->

<?php get_sidebar() ?>
<?php get_footer() ?>