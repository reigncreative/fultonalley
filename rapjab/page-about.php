<?php
	/**
	 * Template Name: About
	 */
?>

<?php get_header() ?>
<?php the_post(); ?>
	<div id="top_image_wrapper" style=" background-image: url('<?=get_thumbnail($post->ID, 'full')?>');">
		<div id="top_image" class="container text-center">
			<h1 id="top_title"><?=get_post_meta($post->ID, 'top_title', true)?></h1>
			<div id="top_text"><?=wpautop(get_post_meta($post->ID, 'top_text', true))?></div>
		</div>
	</div>
	<div id="wrapper" class="container">
		<div class="row">
			<div id="container" class="col-sm-10 col-sm-offset-1">
				<div id="post-<?php the_ID() ?>" <?php post_class(); ?>>
					<h2 class="entry-title normal_page_title"><?php the_title() ?></h2>
					<div class="entry-content normal_page_content">
	                    <?php the_content() ?>
<p>
<h2 style="margin-top:60px;" class="entry-title normal_page_title">Virtual Walk Through Of Our Lanes</h2>
<iframe src="https://www.google.com/maps/embed?pb=!1m0!3m2!1sen!2sus!4v1447268134295!6m8!1m7!1skOTWS89r744AAAQvOX5Scg!2m2!1d29.94693827478117!2d-90.06512143555267!3f205.63!4f0!5f0.7820865974627469" width="100%" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
</p
<p>
<h2 style="margin-top:60px;" class="entry-title normal_page_title">Virtual Walk Through Of Our Games Area</h2>
<iframe src="https://www.google.com/maps/embed?pb=!1m0!3m2!1sen!2sus!4v1447268267531!6m8!1m7!1sSkt09tfhYAsAAAQvOX6b6w!2m2!1d29.94696424966542!2d-90.0652075238412!3f242.51548417165284!4f0.937105853731822!5f0.4000000000000002" width="100%" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
</p>
					</div>
				</div><!-- .post -->
			</div><!-- #container -->
			<?php //get_sidebar() ?>
		</div>
	</div><!-- #wrapper -->
<?php get_footer() ?>