<?php get_header() ?>
	
	<div id="wrapper">
		<div id="container" class="col-md-8">

            <?php the_post() ?>

			<div id="post-<?php the_ID() ?>" <?php post_class(); ?>>
				<h2 class="entry-title"><?php the_title() ?></h2>
				<div class="entry-content">
                    <?php the_content() ?>
				</div>
			</div><!-- .post -->

			<div id="nav-below" class="navigation">
				<div class="nav-previous"><?php previous_post_link( '%link', '<span class="meta-nav">&laquo;</span> %title' ) ?></div>
				<div class="nav-next"><?php next_post_link( '%link', '%title <span class="meta-nav">&raquo;</span>' ) ?></div>
			</div>

            <?php // comments_template() ?>

		</div><!-- #container -->
	</div><!-- #wrapper -->

<?php get_sidebar() ?>
<?php get_footer() ?>