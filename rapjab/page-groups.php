<?php
	/**
	 * Template Name: Groups
	 */
?>

<?php get_header() ?>
<?php the_post(); ?>
	<div id="top_image_wrapper" style=" background-image: url('<?=get_thumbnail($post->ID, 'full')?>');">
		<div id="top_image" class="container text-center">
			<h1 id="top_title"><?=get_post_meta($post->ID, 'top_title', true)?></h1>
			<div id="top_text"><?=wpautop(get_post_meta($post->ID, 'top_text', true))?></div>
			<a href="javascript:void(0);" title="Play Video" class="home_button" id="video_button_groups">Play Video</a>
		</div>
	</div>
	<div id="wrapper" class="container">
		<div id="groups_top">
			<div class="row">
				<div class="col-sm-7">
					<h2 class="groups_title"><?=get_post_meta($post->ID, 'section_1_title', true)?></h2>
					<div class="groups_text"><?=wpautop(get_post_meta($post->ID, 'section_1_text', true))?></div>
				</div>
				<div class="col-sm-4">
					<div id="event_reg_panel">
						<h3>Let’s make some magic happen.</h3>
						<p>
							<i>Call our event planners</i><br />
							<a href="tel:504.208.5593" title="504.208.5593">504.208.5593</a>
						</p>
						<p>
							<i>Email our event planners</i><br />
							<a href="mailto:events@fultonalley.com" title="events@fultonalley.com">events@fultonalley.com</a>
						</p>
						<a href="http://fultonalley.tripleseat.com/party_request/3177" target="_blank" title="Booking Inquiry" id="booking_inquiry">Booking Inquiry</a>
					</div>
				</div>
			</div>
		</div>

		<div id="groups_bottom">
			<div class="row">
				<div class="col-sm-6">
					<?php $image_post = get_post(get_post_meta($post->ID, 'section_2_image', true)); ?>
					<img src="<?=$image_post->guid?>" alt="Groups">
				</div>
				<div class="col-sm-6">
					<div class="menu_detail">
						<h3 class="menu_detail_title"><?=get_post_meta($post->ID, 'section_2_title', true)?></h3>
						<div class="menu_detail_text"><?=wpautop(get_post_meta($post->ID, 'section_2_text', true))?></div>
					</div>
				</div>
			</div>
		</div>
	</div><!-- #wrapper -->
<?php get_footer() ?>