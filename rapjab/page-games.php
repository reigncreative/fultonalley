<?php
	/**
	 * Template Name: Games
	 */
?>
<?php get_header() ?>
<?php the_post(); ?>
	<div id="top_image_wrapper" style=" background-image: url('<?=get_thumbnail($post->ID, 'full')?>');">
		<div id="top_image" class="container text-center">
			<h1 id="top_title"><?=get_post_meta($post->ID, 'top_title', true)?></h1>
			<div id="top_text"><?=wpautop(get_post_meta($post->ID, 'top_text', true))?></div>
			<div class="row" id="prices_wrapper">
				<div class="col-sm-8 col-sm-offset-2">
					<div id="price_listing">
						<div class="row">
							<div class="col-sm-6 text-center price_left">
								<span class="time"><span class="bold">Lane Rental  - <sup>$</sup>30/hr</span></span>
								<span class="day">[<span class="bold">$140 / 2 HRS of reserved lane space & shoes</span><br> Friday &amp; Saturday after 7pm]</span>
							</div>
							<div class="col-sm-6 text-center price_right">
								<span class="time"><span class="bold">Happy Hour Bowling</span></span>
								<span class="day">1st 1/2 hour free & free shoe rental.<br><br></span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="wrapper_games">
		<div id="wrapper" class="container">
			<div id="games">
				<div id="game_1">
					<div id="game_1_inner">
						<ul class="bxslider">
						  <?php for ($i=1; $i<=6; $i++) {
						  	if (get_post_meta($post->ID, 'game_'.$i.'_image', true)) {
						  		$image_post = get_post(get_post_meta($post->ID, 'game_'.$i.'_image', true)); ?>
						  	<li><div class="game_slide" style="background-image: url('<?=$image_post->guid?>');"></div></li>
						  <?php } } ?>
						</ul>
						<div class="game_image_text">
							<h3 class="game_title"><?=get_post_meta($post->ID, 'section_1_title', true)?></h3>
							<div class="game_text"><?=wpautop(get_post_meta($post->ID, 'section_1_text', true))?></div>
						</div>
					</div>
				</div>
				<div id="games_right">
					<div id="game_2">
						<?php $image_post = get_post(get_post_meta($post->ID, 'section_2_image', true)); ?>
						<div id="game_2_inner" style="background-image: url('<?=$image_post->guid?>');">
							<div class="game_image_text">
								<h3 class="game_title"><?=get_post_meta($post->ID, 'section_2_title', true)?></h3>
								<div class="game_text"><?=wpautop(get_post_meta($post->ID, 'section_2_text', true))?></div>
							</div>
						</div>
					</div>
					<div id="game_3">
						<?php $image_post = get_post(get_post_meta($post->ID, 'section_3_image', true)); ?>
						<div id="game_3_inner" style="background-image: url('<?=$image_post->guid?>');">
							<div class="game_image_text">
								<h3 class="game_title"><?=get_post_meta($post->ID, 'section_3_title', true)?></h3>
								<div class="game_text"><?=wpautop(get_post_meta($post->ID, 'section_3_text', true))?></div>
							</div>
						</div>
					</div>
					<div id="game_4">
						<?php $image_post = get_post(get_post_meta($post->ID, 'section_4_image', true)); ?>
						<div id="game_4_inner" style="background-image: url('<?=$image_post->guid?>');">
							<div class="game_image_text">
								<h3 class="game_title"><?=get_post_meta($post->ID, 'section_4_title', true)?></h3>
								<div class="game_text"><?=wpautop(get_post_meta($post->ID, 'section_4_text', true))?></div>
								<a href="http://www.fultonalley.com/private-events/" id="book_a_party" title="Book a Party">Book a Party</a>
							</div>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div><!-- #wrapper -->
	 </div>
<?php get_footer() ?>