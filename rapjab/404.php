<?php get_header() ?>
	
	<div id="wrapper">
		<div id="container" class="col-md-8">
			<div id="content">

				<div id="post-0" class="post error404 not-found">
					<h2 class="entry-title">Not Found</h2>
					<div class="entry-content">
						<p>Apologies, but we were unable to find what you were looking for. Perhaps  searching will help</p>
					</div>
				</div><!-- .post -->

			</div><!-- #content -->
		</div><!-- #container -->
	</div><!-- #wrapper -->

<?php get_sidebar() ?>
<?php get_footer() ?>