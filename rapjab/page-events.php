<?php
	/**
	 * Template Name: Events
	 */
?>

<?php get_header() ?>
<?php the_post(); ?>
	<div id="top_image_wrapper" style=" background-image: url('<?=get_thumbnail($post->ID, 'full')?>');">
		<div id="top_image" class="container text-center">
			<h1 id="top_title"><?=get_post_meta($post->ID, 'top_title', true)?></h1>
			<div id="top_text"><?=wpautop(get_post_meta($post->ID, 'top_text', true))?></div>
		</div>
	</div>
	<div id="wrapper" class="container">
		<div class="row">
			<div id="container" class="col-sm-10">
				<div id="post-<?php the_ID() ?>" <?php post_class(); ?>>
					<div id="events_header">
						<h2 class="first-title">
							<?php if ($_GET['event_month']) { ?>
								<?=date('F', strtotime($_GET['event_month'].'01'))?> Events
							<?php } else { ?>
								<?=get_post_meta($post->ID, 'first_title', true)?>
							<?php } ?>
						</h2>
						<h3 class="second-title"><?=get_post_meta($post->ID, 'second_title', true)?></h3>
					</div>
					<div class="events_list">
	                    <?php 
		                    global $post;
	                        $args = array(
	                            'posts_per_page' => 4,
	                            'post_type'=> 'event',
	                            'paged' => get_query_var('paged'),
	                            'meta_key' => 'date',
	                            'orderby' => 'meta_value',
	                        	'order' => 'ASC',
	                        );

	                        if ($_GET['event_month']) {
	                        	$args['meta_query'][] = array(
                                  'key' => 'date',
                                  'compare' => 'LIKE',
                                  'value' => $_GET['event_month'],
                                );
	                        } else {
	                        	$args['meta_query'][] = array(
                                  'key' => 'date',
                                  'compare' => '>=',
                                  'value' => date('Ymd'),
                                );
	                        }

	                        query_posts( $args );
	                        while ( have_posts() ) : the_post(); ?>
	                        	<div class="event_li">
	                        		<div class="row">
	                        			<div class="col-sm-4 event_pic">
                        					<img src="<?=get_thumbnail($post->ID, 'event_thumb')?>" alt="<?php the_title(); ?>">
	                        			</div>
	                        			<div class="col-sm-8">
	                        				<div class="event_date"><?=date('n.j.y', strtotime(get_post_meta($post->ID, 'date', true)))?><?php if (get_post_meta($post->ID, 'time', true)) { ?> - <?=get_post_meta($post->ID, 'time', true)?><?php } ?></div>
	                        				<h2 class="event_title"><?php the_title(); ?></h2>
	                        				<div class="event_content"><?php the_content(); ?></div>
	                        				<?php
	                        					$website = get_post_meta($post->ID, 'website', true);
	                        					if ($website) { ?>
	                        						<a href="<?=$website?>" title="Learn More" class="event_link" target="_blank">Learn More</a>
	                        					<?php } ?>
	                        			</div>
	                        		</div>
	                        	</div>
	                       	<?php endwhile; ?>
	                        
	                        <div id="nav-below" class="navigation">
	            				<?php wp_pagenavi(); ?>
	                            <div class="clear"></div>
	            			</div>
					</div>
				</div><!-- .post -->
			</div><!-- #container -->
			<?php get_sidebar(); ?>
		</div>
	</div><!-- #wrapper -->
<?php get_footer() ?>